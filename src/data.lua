data.raw["mining-drill"]["pumpjack"].energy_usage = "110kW"
data.raw["mining-drill"]["pumpjack"].mining_speed = 2

data.raw.resource["crude-oil"].autoplace.additional_richness = 20000
data.raw.resource["crude-oil"].autoplace.base_density = 8.2
data.raw.resource["crude-oil"].autoplace.base_spots_per_km2 = 3.0
data.raw.resource["crude-oil"].infinite = false

data.raw.resource["angels-natural-gas"].autoplace.additional_richness = 20000
data.raw.resource["angels-natural-gas"].autoplace.base_density = 8.2
data.raw.resource["angels-natural-gas"].autoplace.base_spots_per_km2 = 3.0
data.raw.resource["angels-natural-gas"].infinite = false
data.raw.resource["angels-natural-gas"].maximum = nil
data.raw.resource["angels-natural-gas"].minimum = 15000
data.raw.resource["angels-natural-gas"].normal = 100000

data.raw.resource["angels-fissure"].autoplace.additional_richness = 20000
data.raw.resource["angels-fissure"].autoplace.base_density = 8.2
data.raw.resource["angels-fissure"].autoplace.base_spots_per_km2 = 3.0
data.raw.resource["angels-fissure"].infinite = false
data.raw.resource["angels-fissure"].maximum = nil
data.raw.resource["angels-fissure"].minimum = 15000
data.raw.resource["angels-fissure"].normal = 100000
