#!/bin/bash

if ! echo "${CI_COMMIT_TAG}" | grep --quiet -P '^\d+\.\d+\.\d+$'; then
    echo "Bad version, needs to be %u.%u.%u"
    exit 1
fi

mkdir -p dist
cp -r src/* dist/
sed -i 's/{{{VERSION}}}/'"${CI_COMMIT_TAG}"'/' dist/info.json
